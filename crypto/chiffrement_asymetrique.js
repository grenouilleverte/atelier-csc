const crypto = require('crypto')

// Generation des clefs prive et public
const keyPair = crypto.generateKeyPairSync('rsa', {modulusLength: 2048});

//console.log(keyPair.publicKey.export({type: 'pkcs1', format: 'pem'}))
//console.log(keyPair.privateKey.export({type: 'pkcs1', format: 'pem'}))


// Chiffrement d'un message
message         = 'Un message ultra secret !'
console.log('Notre message: ', message)

message_chiffre = crypto.publicEncrypt(keyPair.publicKey, Buffer.from(message))
console.log('Le message chiffre: ', message_chiffre.toString('hex'))

message_dechiffre = crypto.privateDecrypt(keyPair.privateKey, message_chiffre)
console.log('Le message dechiffre: ', message_dechiffre.toString())
