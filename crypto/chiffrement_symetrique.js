var crypto = require('crypto');

const inputEncoding = 'utf8';
const outputEncoding = 'hex';

const algorithm = 'aes256';
const password = 'Password used to generate key';
const key = crypto.scryptSync(password, 'salt', 24);


const message = 'Un message super secret !';

// Chiffrement du message

iv = Buffer.alloc(16, 0) // initialization vector

console.log('iv:', iv.toString('hex'))

var chiffre = crypto.createCipheriv(algorithm, key, iv);
var message_chiffre = chiffre.update(message, inputEncoding, outputEncoding)
message_chiffre += chiffre.final(outputEncoding)

console.log('message chiffre: ', message_chiffre)

//Dechiffrement du message
var dechiffre = crypto.createDecipheriv(algorithm, key, iv)
var messsage_dechiffre = dechiffre.update(message_chiffre, outputEncoding, inputEncoding)
message_dechiffre += dechiffre.final(inputEncoding)

console.log('message dechiffre: ', message_dechiffre);
