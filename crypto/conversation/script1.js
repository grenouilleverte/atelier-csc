const crypto    = require('crypto')
const fs        = require('fs');

const yannick   = crypto.generateKeyPairSync('rsa', {modulusLength: 512});
const toffer    = crypto.generateKeyPairSync('rsa', {modulusLength: 512});

function generateSignature(message, privateKey) {
    var sign = crypto.createSign('SHA256')
    sign.write(message)
    sign.end()
    signature = sign.sign(privateKey, 'hex');
    return signature
}

messages_list = ['Salut Yannick', 'Salut Toffer !', 'tu vas bien ?', 'oui super et toi ?', 'ouai super', 'est-ce que tu peux me donner ta voiture ?', 'oui bien sur tu peux la prendre', 'merci trop cool !']

fs.writeFileSync('yannick_public_key.pem', yannick.publicKey.export({type: 'pkcs1', format: 'pem'}))
fs.writeFileSync('toffer_public_key.pem', toffer.publicKey.export({type: 'pkcs1', format: 'pem'}))

var stream = fs.createWriteStream("conversation.txt");
stream.once('open', function(fd) {

    for (idx in messages_list) {
        if (idx % 2 === 0) {
            signature = generateSignature(messages_list[idx], toffer.privateKey)
            message = 'Toffer: ' + messages_list[idx]
            stream.write(message+'\n')
            stream.write(signature+'\n')
        }
        else {
            signature = generateSignature(messages_list[idx], yannick.privateKey)
            message = 'Yannick: ' + messages_list[idx]
            stream.write(message+'\n')
            stream.write(signature+'\n')
        }
        stream.write('\n')
    }
    stream.write('SIGNED USING SHA256\n')
    stream.end()
})

//var verify = crypto.createVerify('SHA256')
//verify.update(message)
//verify.end()
//isValid = verify.verify(keyPair2.publicKey, signature, 'hex')
//console.log(isValid)
