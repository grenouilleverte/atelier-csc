// https://justinboyerwriter.com/2017/09/26/the-developers-guide-to-encryption-aes-in-node-js/
const crypto = require('crypto')


const password  = 'un mot de passe tres dur pour generer la clef'
const sel       = crypto.randomBytes(16)
const key       = generateKey(password, sel)

const algo      = 'aes-256-cbc'
const iv        = crypto.randomBytes(16) // initialization vector

const codage_clair     = 'utf8'
const codage_chiffre    = 'hex'

function generateKey(password, sel) {
    return crypto.pbkdf2Sync(Buffer.from(password), sel, 174523, 32, 'sha512')
}

function chiffrer(message) {
    var chiffre = crypto.createCipheriv(algo, key, iv)
    var message_chiffre = chiffre.update(message, codage_clair, codage_chiffre)
    message_chiffre += chiffre.final(codage_chiffre)
    return message_chiffre
}

function dechiffrer(message) {
    var dechiffre = crypto.createDecipheriv(algo, key, iv)
    var message_dechiffre = dechiffre.update(message, codage_chiffre, codage_clair)
    message_dechiffre += dechiffre.final(codage_clair)
    return message_dechiffre
}

// demo
const message = 'AAAAAAAAAAAAAAA'
const message_chiffre = chiffrer(message)
console.log('message chiffre: ', message_chiffre)

const message_dechiffre = dechiffrer(message_chiffre)
console.log('message dechiffre: ', message_dechiffre)

const message2 = 'AAAAAAAAAAAAAAB'
const message2_chiffre = chiffrer(message2)
console.log('message chiffre: ', message2_chiffre)

const message2_dechiffre = dechiffrer(message2_chiffre)
console.log('message dechiffre: ', message2_dechiffre)
