
const crypto = require('crypto')

function sha256(toDigest) {

    const hash = crypto.createHash('SHA256');
    digested = hash.update(toDigest, 'utf8')
                   .digest('hex')
    return digested
}

const message = "Un message que je vais signer."
const keyPair = crypto.generateKeyPairSync('rsa', {modulusLength: 2048});
const keyPair2 = crypto.generateKeyPairSync('rsa', {modulusLength: 2048});

var sign = crypto.createSign('SHA256')
sign.write(message)
sign.end()

const signature = sign.sign(keyPair.privateKey, 'hex');

console.log(signature)

var verify = crypto.createVerify('SHA256')
verify.update(message)
verify.end()
isValid = verify.verify(keyPair2.publicKey, signature, 'hex')
console.log(isValid)
