from PIL import Image
import numpy as np
import random

img = Image.open('original.jpeg')
img = np.asarray(img, dtype='uint8')

def set_bit(v, index, x):
  """Set the index:th bit of v to 1 if x is truthy, else to 0, and return the new value."""
  mask = 1 << index   # Compute mask, an integer with just bit 'index' set.
  v &= ~mask          # Clear the bit indicated by the mask (if x is False)
  if x:
    v |= mask         # If x was True, set the bit indicated by the mask.
  return v            # Return the result, we're done.


for i in range(0, 20):
    img_cpy = img.copy()
    layer = img_cpy[:,:,i%3]
    x_max = layer.shape[0]
    y_max = layer.shape[1]
    for j in range(0, 5):
        x = random.randint(0, x_max-1)
        y = random.randint(0, y_max-1)
        index = 0
        bit_val = layer[x][y]&2**index
        layer[x][y] = set_bit(layer[x][y], index, bit_val ^ 1)

    img_mod = Image.fromarray(img_cpy)
    img_mod.save("image%d.png" % i)
